import UIKit

var str = "Hello world"

var numbers = [1, 2, 3, 4, 5]

let sum = numbers.reduce(0) { (result, next) in
    return result + next
}

let sum2 = numbers.reduce(0) { $0 + $1 }
sum2

let dictionary = [
    "name": "Nicolas",
    "lastname": "Inchiglema",
    "jobTitle": "Estudent"
]

numbers[0]
dictionary["name"]

let temperature = 15

switch temperature {
case 1...12 where temperature % 2 == 0:
    print("It's cold! ❄️")
case 13...20:
    print("It's regular 😒")
case 21...37:
    print("It's hot! 😎")
case 38...60:
    print("You are a chiken! 🍗🥵")
default:
    print("--IDK--")
}

func myFunction() {
    print("Hi, you MTF!")
}

myFunction()

func square(num: Int) -> Int {
    return num * num
}

var numb: Int = 2
print("square (2) = ", square(num: numb))

func concat(_ Message: String, to receptor: String) {  // to is used for call function and receptor in the function
    // _ it hide the tag when you ca
    print(Message, receptor)
}

var s1 = "Hi How are you?"
var s2 = "CC"

concat(s1, to: s2)


func sSum (n1: Int, n2: Int, n3: Int = 0) -> Int {
    //Int = 0 permite que esa variable sea opcional
    return n1 + n2 + n3
}



