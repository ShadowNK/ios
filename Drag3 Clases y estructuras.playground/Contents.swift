import UIKit

// Clases y Estructuras

struct car {
    var maxSpeed:Double
    var brand:String
    
    func maxSmll() {
        print("Max speed in milles: ", (maxSpeed * 1.2))
    }
    
//      NO SE PUEDE TENER FUNCIONES QUE MODIFIQUEN LA DATA DEL STRUCT
//    func increase() {
//        maxSpeed += 10
//    }
}

let carA = car(maxSpeed: 200, brand: "VW")

carA.maxSmll()


class Person {
    let name:String
    let lastName:String
    let nId:String
    
    init(name: String, lastName: String, nId: String) {
        self.name = name
        self.lastName = lastName
        self.nId = nId
    }
}

protocol Friendable {
    var friend: Person? { get set }
    func addFirned (friend: Person)
}

//Herencia

class Employee: Person, Friendable {
    let jobTitle: String
    var salary: Double?
    var friend: Person?
    
    init(name: String, lastName: String, nId: String, jobTitle: String) {
        self.jobTitle = jobTitle
        super.init(name: name, lastName: lastName, nId: nId)
    }
    
    func addFirned(friend: Person) {
         
    }
}

func polimorphism(person: Person) {
    print(person.name)
}

let employee = Employee(name: "Andres", lastName: "Duran", nId: "9876543210", jobTitle: "Software engineer")

polimorphism(person: employee)

var myOptional: Int?

print(myOptional)

myOptional = 10

func somethingCool(number: Int) {
    print(number)
}

somethingCool(number: myOptional ?? 0) // ?? -> en caso de que sea null pasa

func otherSomething(number: Int?) {
    if let isNumber = number, isNumber % 2 == 0 {
        print(isNumber)
    }
}

func moreOptionals(number: Int?) {
    guard let isNumber = number else {
        print("null")
        return
    }
    print(isNumber)
}

print(myOptional)

print(myOptional!)

myOptional = nil

//print(myOptional!)

