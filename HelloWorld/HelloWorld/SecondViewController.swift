//
//  SecondViewController.swift
//  HelloWorld
//
//  Created by ShadowNK on 10/18/19.
//  Copyright © 2019 ShadowNK. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet weak var lblName: UILabel!
    
    var customTitle: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if customTitle != "" {
            lblName.text = customTitle
        }
    }

    @IBAction func btnNumOne(_ sender: Any) {
        lblName.text = "1st view controller"
    }
    
    @IBAction func btnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
