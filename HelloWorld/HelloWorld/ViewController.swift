//
//  ViewController.swift
//  HelloWorld
//
//  Created by ShadowNK on 10/1/19.
//  Copyright © 2019 ShadowNK. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var lblHW: UILabel!
    
    @IBOutlet weak var tfText: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

   
    @IBAction func btnSUpress(_ sender: Any) {
        lblHW.text = tfText.text
        tfText.text = ""
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToSecondViewController" {
            let destinationView = segue.destination as! SecondViewController
            destinationView.customTitle = tfText.text
        }
    }
    
}

